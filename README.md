# OpenML dataset: Smogon-6v6-Pokemon-Tiers

https://www.openml.org/d/43694

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset stems from Alberto Barradas' popular Pokemon with stats dataset by listing the tiered Pokemon in Smogon 6v6.
Smogon 6v6 is one of the most popular formats for competitive Pokemon. Although it is not the "official" competitive format, there is still a significant number of people who play the format. There are a number of 'tiers' of Pokemon in which people can play, the most popular being OU. This dataset seeks to display both a Pokemon's stats and corresponding tier for easier competitive analysis.
In addition to the addition of the 'Tier' variable, there are several other changes I made to the set:

Classified Mythical Pokemon as 'Legendary'
Changed the naming convention of Mega Evolutions and some form changes
Addition of 'Mega' tier to signify Mega Evolutions

Note that this dataset includes only Pokemon tiered from PU to AG. NFE and LC Pokemon are not included unless they appear in Smogon's list. List of which Pokemon are in which tier was found here.
Thank you to Alberto Barradas for his comprehensive Pokemon dataset.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43694) of an [OpenML dataset](https://www.openml.org/d/43694). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43694/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43694/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43694/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

